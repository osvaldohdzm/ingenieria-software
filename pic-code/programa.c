

#include <16f877a.h>
#fuses hs,nowdt,noprotect,noput
#use delay (clock=4M)

#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

char dato = "";

void main()
{
   while(true)
   {
      delay_ms(1000);
      dato = getc();
      printf("%u\n\r",dato); 
      
   }
}


