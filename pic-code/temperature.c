#include <16f877A.h>
#device adc=10 //ADC de 10 bits
#include <stdio.h>
#fuses XT, NOWDT, NOPROTECT, HS, NOPUT
//#use delay (clock=20M)
#use delay (clock=4M)
#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

float read_value = 0;
float h1 = 0;
float h2 = 0;
float rtd = 0;

void main() 
{  
 
 int counter = 0;

  setup_adc_ports(AN0_AN1_AN3);
  setup_adc(adc_clock_div_64);
  
  
  while (true)
  {   
  
     if (counter == 0)
     {
     set_adc_channel(0);
      read_value = read_adc() * 5 * 100 / 1024;
      h1 = read_value;
      delay_ms(500); 
      printf("%.2f\r\n",read_value +1000);
      delay_ms(1000); 
      counter = 1;
     }
     
  
    if (counter == 1)
     {
     set_adc_channel(1);
      read_value = read_adc() * (1/1000) * 100 /3;
    delay_ms(500); 
    printf("%.2f\r\n",read_value + 1000);
    delay_ms(1000); 
    counter = 0;
     }
         
  }

}

